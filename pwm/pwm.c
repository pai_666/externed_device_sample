#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define PWM_CHIP_PATH "/sys/class/pwm/pwmchip0/"
#define PWM_EXPORT_PATH PWM_CHIP_PATH "export"
#define PWM_UNEXPORT_PATH PWM_CHIP_PATH "unexport"

#define PWM1_PERIOD_PATH PWM_CHIP_PATH "pwm1/period"
#define PWM1_DUTY_CYCLE_PATH PWM_CHIP_PATH "pwm1/duty_cycle"
#define PWM1_ENABLE_PATH PWM_CHIP_PATH "pwm1/enable"

#define PWM15_PERIOD_PATH PWM_CHIP_PATH "pwm15/period"
#define PWM15_DUTY_CYCLE_PATH PWM_CHIP_PATH "pwm15/duty_cycle"
#define PWM15_ENABLE_PATH PWM_CHIP_PATH "pwm15/enable"


void export_pwm(int num) {
    int export_fd = open(PWM_EXPORT_PATH, O_WRONLY);
    if (export_fd == -1) {
        perror("Error opening export");
        exit(EXIT_FAILURE);
    }

    // if (write(export_fd, "1", 2) == -1) {
    //     perror("Error exporting PWM");
    //     close(export_fd);
    //     exit(EXIT_FAILURE);
    // }
    
    dprintf (export_fd, "%d", num);

    close(export_fd);
}

void disable_pwm(int num);
void unexport_pwm(int num) {
    int unexport_fd = open(PWM_UNEXPORT_PATH, O_WRONLY);
    if (unexport_fd == -1) {
        perror("Error opening unexport");
        exit(EXIT_FAILURE);
    }

    // if (write(unexport_fd, "1", 2) == -1) {
    //     perror("Error unexporting PWM");
    //     close(unexport_fd);
    //     exit(EXIT_FAILURE);
    // }
    disable_pwm(num);
    dprintf (unexport_fd, "%d", num);

    close(unexport_fd);
}

void set_pwm_period(int num, int period) {
    int period_fd;
    switch (num) {
        case 1:
            period_fd = open(PWM1_PERIOD_PATH, O_WRONLY);
            break;
        case 15:
            period_fd = open(PWM15_PERIOD_PATH, O_WRONLY);
            break;
        default:
            period_fd = open(PWM1_PERIOD_PATH, O_WRONLY);
    }
    
    
    if (period_fd == -1) {
        perror("Error opening period");
        exit(EXIT_FAILURE);
    }

    dprintf(period_fd, "%d", period);

    close(period_fd);
}

void set_pwm_duty_cycle(int num, int duty_cycle) {
    int duty_cycle_fd;
    switch (num) {
        case 1:
            duty_cycle_fd = open(PWM1_DUTY_CYCLE_PATH, O_WRONLY);
            break;
        case 15:
            duty_cycle_fd = open(PWM15_DUTY_CYCLE_PATH, O_WRONLY);
            break;
        default:
            duty_cycle_fd = open(PWM1_DUTY_CYCLE_PATH, O_WRONLY);
    }
    
    if (duty_cycle_fd == -1) {
        perror("Error opening duty cycle");
        exit(EXIT_FAILURE);
    }

    dprintf(duty_cycle_fd, "%d", duty_cycle);

    close(duty_cycle_fd);
}

void enable_pwm(int num) {
    int enable_fd;
    switch (num) {
        case 1:
            enable_fd = open(PWM1_ENABLE_PATH, O_WRONLY);
            break;
        case 15:
            enable_fd = open(PWM15_ENABLE_PATH, O_WRONLY);
            break;
        default:
            enable_fd = open(PWM1_ENABLE_PATH, O_WRONLY);
    }

    if (enable_fd == -1) {
        perror("Error opening enable");
        exit(EXIT_FAILURE);
    }

    if (write(enable_fd, "1", 2) == -1) {
        perror("Error enabling PWM");
        close(enable_fd);
        exit(EXIT_FAILURE);
    }

    close(enable_fd);
}

void disable_pwm(int num) {
    int enable_fd;
    switch (num) {
        case 1:
            enable_fd = open(PWM1_ENABLE_PATH, O_WRONLY);
            break;
        case 15:
            enable_fd = open(PWM15_ENABLE_PATH, O_WRONLY);
            break;
        default:
            enable_fd = open(PWM1_ENABLE_PATH, O_WRONLY);
    }

    if (enable_fd == -1) {
        perror("Error opening enable");
        exit(EXIT_FAILURE);
    }

    if (write(enable_fd, "0", 2) == -1) {
        perror("Error disabling PWM");
        close(enable_fd);
        exit(EXIT_FAILURE);
    }

    close(enable_fd);
}

void help () {
    printf ("==========Usage of Demo_pwm==========\n");
    printf ("./Demo_pwm to see this help\n");
    printf ("./Demo_pwm <1> <2> <3> <4>\n");
    printf ("<1> be open or close to enable/disable PWM\n");
    printf ("<2> be 1 or 15 to chose PWM0_1 or PWM0_15\n");
    printf ("<3> be value for period\n");
    printf ("<3> be value for duty_cycle\n");
    printf ("example:./Demo_pwm open 1 50000 25000\n");
    printf ("example:./Demo_pwm close 1\n");
}

int main(int argc, char** argv) {

    if ((argc == 3) && !strcmp(argv[1], "close")) {
        int num = atoi (argv[2]);
        unexport_pwm(num);
        return 0;
    }
    else if ((argc == 5) && !strcmp(argv[1], "open")) {
        int num = atoi (argv[2]);
        int period = atoi (argv[3]);
        int duty_cycle = atoi (argv[4]);
        export_pwm(num);
        set_pwm_period(num, period);
        set_pwm_duty_cycle(num, duty_cycle);
        enable_pwm(num);
    }
    else {
        help ();        
    }
    

    return 0;
}