/*
 * Description:  Seneasy Test BS25
 * Author: @yuanhanyi
 * Create: 2023-10-25
 */
#ifndef SLE_RCU_CLIENT_H
#define SLE_RCU_CLIENT_H

#include "sle_ssap_client.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

 #define USB_HID_RCU_MAX_KEY_LENTH      6
typedef struct usb_hid_rcu_keyboard_report {
    uint8_t kind;
    uint8_t special_key;                         /*!< 8bit special key(Lctrl Lshift Lalt Lgui Rctrl Rshift Ralt Rgui) */
    uint8_t reserve;
    uint8_t key[USB_HID_RCU_MAX_KEY_LENTH]; /*!< Normal key */
} usb_hid_rcu_keyboard_report_t;

typedef union mouse_key {
    struct {
        uint8_t left_key   : 1;
        uint8_t right_key  : 1;
        uint8_t mid_key    : 1;
        uint8_t reserved   : 5;
    } b;
    uint8_t d8;
} mouse_key_t;

/**
 * @if Eng
 * @brief Definitaion of usb hid rcu report struct.
 * @else
 * @brief 定义USB MOUSE HID上报的结构体。
 * @endif
 */
typedef struct usb_hid_rcu_mouse_report {
    uint8_t kind;
    mouse_key_t key;
    int8_t x;                 /* A negative value indicates that the mouse moves left. */
    int8_t y;                 /* A negative value indicates that the mouse moves up. */
    int8_t wheel;             /* A negative value indicates that the wheel roll forward. */
} usb_hid_rcu_mouse_report_t;

/**
 * @if Eng
 * @brief Definitaion of usb hid rcu report struct.
 * @else
 * @brief 定义USB CONSUMER HID上报的结构体。
 * @endif
 */
typedef struct usb_hid_rcu_consumer_report {
    uint8_t kind;
    uint8_t comsumer_key0;
    uint8_t comsumer_key1;
} usb_hid_rcu_consumer_report_t;

void sle_rcu_client_init(ssapc_notification_callback notification_cb, ssapc_indication_callback indication_cb);
void sle_rcu_start_scan(void);
uint16_t get_sle_rcu_conn_id(void);
ssapc_write_param_t *get_sle_rcu_send_param(void);
uint8_t get_ssap_find_ready(void);
uint8_t get_sle_rcu_get_connect_state(void);
uint8_t get_ssap_connect_param_update_ready(void);

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */

#endif