# awe-63

## 一、例程简介
awe-63例程主要基于ws63开发板的sle模块和遥控器通讯，并在oled上显示连接状态，led扩展板上显示通过led同步按键效果

## 二、测试环境说明

### 1) 硬件准备

- WS63开发板
- HiSpark WiFi loT OLED扩展板
- HiSpark WiFi loT LED扩展板

### 2) 软件环境

- MabaXterm
- SSCOM

## 三、硬件连接

如图所示为ws63和扩展板的硬件连接图。

<img src="./README.assets/image-20240130163038462.png" alt="image-20240130163038462" style="zoom:50%;" />


## 四、例程运行说明

1. WS63 开发板套件启动，启动中 oled 扩展板上显示“Hi World
WS63 !!!”，此时扩展板上的红灯亮起，表示为 SLE 未连接状态。如下图所示：
<img src="./README.assets/image-20240130164201797.png" alt="image-20240130164201797" style="zoom:50%;" />

2. 先通过手机蓝牙搜索、匹配“HDRC-BV1”遥控器设备，并确保
与遥控器连接上，此时按遥控器按键，遥控器会亮绿灯。如果蓝
牙遥控器没有与手机连接之前，按遥控器按键则遥控器会亮红灯。

3. 切换 SLE 链路，参考下图按遥控器上的“看机顶盒 TB”切换到
SLE，此时按遥控器按键，遥控器的指示灯是黄色。如下图所示：
<img src="./README.assets/image-20240130164201798.png" alt="image-20240130164201798" style="zoom:50%;" />

4. 遥控器与 WS63 开发板套件通过 SLE 连接成功后，Oled 显示屏
会显示“SLE Remote Connected !”。如下图所示：
<img src="./README.assets/image-20240130164201799.png" alt="image-20240130164201799" style="zoom:50%;" />

5. 按下遥控器的按键，此时扩展板的蓝灯亮，松开遥控器按键则绿
灯 亮 ，同时 oled 的屏幕上显示 遥 控 器 对 应 的 按 键 值“KeyValue:xx”。



