#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <math.h>
#include <unistd.h>
#include <signal.h>
#include <limits.h>
#include <errno.h>

#include <linux/fb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/prctl.h>

#include "securec.h"
#include "loadbmp.h"

#include "gfbg.h"
#include "ot_common_tde.h"
#include "ss_mpi_tde.h"
#include "sample_comm.h"

#define unused __attribute__((unused))

#define FILE_LENGTH_MAX 12
#define CMAP_LENGTH_MAX 256
#define WIDTH_1080P 1920
#define HEIGHT_1080P 1080
#define WIDTH_800 800
#define HEIGHT_600 600

#define SAMPLE_IMAGE_WIDTH     300
#define SAMPLE_IMAGE_HEIGHT    150
#define SAMPLE_IMAGE_NUM       20
#define GFBG_RED_1555          0xFC00
#define GFBG_RED_8888          0xFFff0000
#define WIDTH_1920             1920
#define HEIGHT_1080            1080

#define GRAPHICS_LAYER_G0      0
#define GRAPHICS_LAYER_G1      1
#define GRAPHICS_LAYER_G2      2
#define GRAPHICS_LAYER_G3      3

static unused struct fb_bitfield g_r16 = {10, 5, 0};
static unused struct fb_bitfield g_g16 = {5, 5, 0};
static unused struct fb_bitfield g_b16 = {0, 5, 0};
static unused struct fb_bitfield g_a16 = {15, 1, 0};

static unused struct fb_bitfield g_a32 = {24, 8, 0};
static unused struct fb_bitfield g_r32 = {16, 8, 0};
static unused struct fb_bitfield g_g32 = {8,  8, 0};
static unused struct fb_bitfield g_b32 = {0,  8, 0};

static unused struct fb_bitfield g_a4 = {0, 0, 0};
static unused struct fb_bitfield g_r4 = {0, 4, 0};
static unused struct fb_bitfield g_g4 = {0, 4, 0};
static unused struct fb_bitfield g_b4 = {0, 4, 0};

ot_vo_intf_type g_vo_intf_type = OT_VO_INTF_BT1120;
osd_color_format g_osd_color_fmt = OSD_COLOR_FORMAT_RGB1555;

#if (defined(CONFIG_OT_GFBG_SUPPORT) && defined(CONFIG_OT_VO_SUPPORT))
#define GFBG_BE_WITH_VO    1
#else
#define GFBG_BE_WITH_VO    0
#endif

typedef struct {
    ot_vo_dev vo_dev;
    ot_vo_intf_type vo_intf_type;
}vo_device_info;

typedef struct {
    td_s32 fd; /* fb's file describe */
    td_s32 layer; /* which graphic layer */
    td_s32 ctrlkey; /* {0,1,2,3}={1buffer, 2buffer, 0buffer pan display, 0buffer refresh} */
    td_bool compress; /* image compressed or not */
    ot_fb_color_format color_format; /* color format. */
} pthread_gfbg_sample_info;


void sigint_handler(int signum) {}
static td_void sample_gfbg_to_exit(td_void)
{
	signal(SIGINT, sigint_handler);
	printf("Waiting for Ctrl+C...\n");
    pause();

    return;
}

static td_s32 sample_gfbg_start_vo(vo_device_info *vo_dev_info)
{
#if GFBG_BE_WITH_VO
    ot_vo_intf_type gfbg_vo_intf_type = vo_dev_info->vo_intf_type;
    g_vo_intf_type = vo_dev_info->vo_intf_type;
    ot_vo_dev vo_dev = vo_dev_info->vo_dev;
    ot_vo_pub_attr pub_attr;
    td_u32  vo_frm_rate;
    ot_size size;
    td_s32 ret;
    sample_vo_cfg vo_config = {0};

    /* step 1(start vo):  start vo device. */
    pub_attr.intf_type = gfbg_vo_intf_type;
    pub_attr.intf_sync = OT_VO_OUT_1080P60;
    pub_attr.bg_color = COLOR_RGB_BLUE;
    ret = sample_comm_vo_get_width_height(pub_attr.intf_sync, &size.width, &size.height, &vo_frm_rate);
    if (ret != TD_SUCCESS) {
        sample_print("get vo width and height failed with %d!\n", ret);
        return ret;
    }
    ret = sample_comm_vo_start_dev(vo_dev, &pub_attr, &vo_config.user_sync, vo_config.dev_frame_rate);
    if (ret != TD_SUCCESS) {
        sample_print("start vo device failed with %d!\n", ret);
        return ret;
    }
    /*
     * step 2(start vo): bind G3 to VO device.
     * do this after VO device started.
     * set bind relationship.
     */
    ret = ss_mpi_vo_unbind_layer(OT_VO_LAYER_G3, vo_dev);
    if (ret != TD_SUCCESS) {
        sample_print("un_bind_graphic_layer failed with %d!\n", ret);
        return ret;
    }

    ret = ss_mpi_vo_bind_layer(OT_VO_LAYER_G3, vo_dev);
    if (ret != TD_SUCCESS) {
        sample_print("bind_graphic_layer failed with %d!\n", ret);
        return ret;
    }
    /* step 3(start vo): start hdmi device. */
    if (gfbg_vo_intf_type == OT_VO_INTF_HDMI) {
        sample_comm_vo_hdmi_start(pub_attr.intf_sync);
    }
    /* if it's displayed on bt1120, we should start bt1120 */
    if (gfbg_vo_intf_type == OT_VO_INTF_BT1120) {
        sample_comm_vo_bt1120_start(vo_dev, &pub_attr);
    }
    return TD_SUCCESS;
#else
    return TD_SUCCESS;
#endif
}

static td_s32 sample_init_var(pthread_gfbg_sample_info *info)
{
    struct fb_var_screeninfo var;

    if (ioctl(info->fd, FBIOGET_VSCREENINFO, &var) < 0) {
        sample_print("get variable screen info failed!\n");
        return TD_FAILURE;
    }

    switch (info->color_format) {
        case OT_FB_FORMAT_ARGB8888:
            var.transp = g_a32;
            var.red = g_r32;
            var.green = g_g32;
            var.blue = g_b32;
            var.bits_per_pixel = 32; /* 32 for 4 byte */
            g_osd_color_fmt = OSD_COLOR_FORMAT_RGB8888;
            break;
        default:
            var.transp = g_a16;
            var.red = g_r16;
            var.green = g_g16;
            var.blue = g_b16;
            var.bits_per_pixel = 16; /* 16 for 2 byte */
            break;
    }

    switch (info->ctrlkey) {
        case 3: /* 3 mouse case */
            var.xres_virtual = 48; /* 48 for alg data */
            var.yres_virtual = 48; /* 48 for alg data */
            var.xres = 48; /* 48 for alg data */
            var.yres = 48; /* 48 for alg data */
            break;
        default:
            var.xres_virtual = WIDTH_1080P;
            var.yres_virtual = HEIGHT_1080P * 2; /* 2 for 2buf */
            var.xres = WIDTH_1080P;
            var.yres = HEIGHT_1080P;
    }
    var.activate       = FB_ACTIVATE_NOW;

    if (ioctl(info->fd, FBIOPUT_VSCREENINFO, &var) < 0) {
        sample_print("put variable screen info failed!\n");
        return TD_FAILURE;
    }
    return TD_SUCCESS;
}

static td_s32 sample_init_frame_buffer(pthread_gfbg_sample_info *info, const char *input_file)
{
    td_bool show;
    ot_fb_point point = {0, 0};
    td_char file[PATH_MAX + 1] = {0};

    if (strlen(input_file) > PATH_MAX || realpath(input_file, file) == TD_NULL) {
        return TD_FAILURE;
    }
    /* step 1. open framebuffer device overlay 0 */
    info->fd = open(file, O_RDWR, 0);
    if (info->fd < 0) {
        perror("Error opening file");
        sample_print("open %s failed!\n", file);
        return TD_FAILURE;
    }

    show = TD_FALSE;
    if (ioctl(info->fd, FBIOPUT_SHOW_GFBG, &show) < 0) {
        sample_print("FBIOPUT_SHOW_GFBG failed!\n");
        close(info->fd);
        info->fd = -1;
        return TD_FAILURE;
    }

    printf("filename:%s\n",file);

    /* step 2. set the screen original position */
    switch (info->ctrlkey) {
        case 3: /* 3 mouse case */
            point.x_pos = 150; /* 150 x pos */
            point.y_pos = 150; /* 150 y pos */
            break;
        default:
            point.x_pos = 0;
            point.y_pos = 0;
    }

    if (ioctl(info->fd, FBIOPUT_SCREEN_ORIGIN_GFBG, &point) < 0) {
        sample_print("set screen original show position failed!\n");
        close(info->fd);
        info->fd = -1;
        return TD_FAILURE;
    }

    return TD_SUCCESS;
}

// TODO: 优化函数
static const char* sample_get_file_name(pthread_gfbg_sample_info *info)
{
    const char* filename_list[]={
        "/dev/fb0",
        "/dev/fb1",
        "/dev/fb2",
        "/dev/fb2",
    };
    if(info->layer>=GRAPHICS_LAYER_G0&&info->layer<=GRAPHICS_LAYER_G3)
        return filename_list[info->layer];
    else
        return "/dev/fb0";
}

static td_void sample_gfbg_stop_vo(vo_device_info *vo_dev_info)
{
#if GFBG_BE_WITH_VO
    ot_vo_intf_type gfbg_vo_intf_type = vo_dev_info->vo_intf_type;
    ot_vo_dev vo_dev = vo_dev_info->vo_dev;

    if (gfbg_vo_intf_type == OT_VO_INTF_HDMI) {
        sample_comm_vo_hdmi_stop();
    }
    sample_comm_vo_stop_dev(vo_dev);
    return;
#else
    return;
#endif
}

static int sample_gfbg_pandisplay_init(pthread_gfbg_sample_info *info)
{
    const char* file = sample_get_file_name(info);

    if (sample_init_frame_buffer(info, file) != TD_SUCCESS) {
        return -1;
    }

    if (sample_init_var(info) != TD_SUCCESS) {
        close(info->fd);
        info->fd = -1;
        return -1;
    }

    td_bool show;
    struct fb_var_screeninfo var;

    if (ioctl(info->fd, FBIOGET_VSCREENINFO, &var) < 0) {
        sample_print("get variable screen info failed!\n");
        return TD_FAILURE;
    }

    show = TD_TRUE;
    if (ioctl(info->fd, FBIOPUT_SHOW_GFBG, &show) < 0) {
        sample_print("FBIOPUT_SHOW_GFBG failed!\n");
        return TD_FAILURE;
    }

    // close(info->fd);
    return 0;
}

static td_s32 sample_gfbg_standard_mode(vo_device_info *vo_dev_info)
{
    td_s32 ret;
    pthread_gfbg_sample_info info0;
    ot_vo_dev vo_dev = vo_dev_info->vo_dev;
    ot_vb_cfg vb_conf;

    /* step  1: init variable */
    if (memset_s(&vb_conf, sizeof(ot_vb_cfg), 0, sizeof(ot_vb_cfg)) != EOK) {
        sample_print("%s:%d:memset_s failed\n", __FUNCTION__, __LINE__);
        return TD_FAILURE;
    }
    /* step 2: mpp system init. */
    ret = sample_comm_sys_init(&vb_conf);
    if (ret != TD_SUCCESS) {
        sample_print("system init failed with %d!\n", ret);
        return ret;
    }
    /*
     * step 3: start VO device.
     * NOTE: step 3 is optional when VO is running on other system.
     */
    ret = sample_gfbg_start_vo(vo_dev_info);
    if (ret != TD_SUCCESS) {
        sample_print("VO device %d start failed\n", vo_dev_info->vo_dev);
        goto sample_gfbg_standard_mode_0;
    }
    /* step 4:  start gfbg. */
    info0.layer = vo_dev;    /* VO device number */
    info0.fd = -1;
    info0.ctrlkey = 2; /* 2 none buffer */
    info0.compress = TD_FALSE; /* compress opened or not */
    info0.color_format = OT_FB_FORMAT_ARGB8888;
    sample_gfbg_pandisplay_init(&info0);

    sample_gfbg_to_exit();

    sample_gfbg_stop_vo(vo_dev_info);
sample_gfbg_standard_mode_0:
    sample_comm_sys_exit();
    return ret;
}

int main()
{
    vo_device_info vo_dev_info;
    vo_dev_info.vo_dev = SAMPLE_VO_DEV_DHD0;
    vo_dev_info.vo_intf_type = OT_VO_INTF_HDMI;

	sample_gfbg_standard_mode(&vo_dev_info);

	return 0;
} 