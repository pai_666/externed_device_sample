# externed_device_sample

#### 介绍
欧拉派海鸥系列驱动测试程序.其中mpp为媒体相关sample，other为非媒体相关sample。

## 编译说明
测试工程有两种编译方式。
1）通过yocto工程构建文件系统。
    [构建方法](https://openeuler.gitee.io/yocto-meta-openeuler/master/bsp/arm64/hieulerpi/hieulerpi.html)

2）在x86的Ubuntu服务器搭建交叉编译环境进行编译。

2.1）下载并安装交叉编译工具链
```
假设当前目录/home/xxx/
wget http://121.36.84.172/dailybuild/EBS-openEuler-23.09/EBS-openEuler-23.09/embedded_img/aarch64/aarch64-qemu/openeuler-glibc-x86_64-openeuler-image-aarch64-qemu-aarch64-toolchain-23.09.sh

注意：请使用欧拉文件系统构建配套的交叉编译工具链。

mkdir toolchain
./openeuler-glibc-x86_64-openeuler-image-aarch64-qemu-aarch64-toolchain-23.09.sh
openEuler Embedded(openEuler Embedded Reference Distro) SDK installer version 23.09
===================================================================================
Enter target directory for SDK (default: /opt/openeuler/oecore-x86_64): /home/xxx/toolchain
You are about to install the SDK to "/home/robot/hirobot_support/toolchain". Proceed [Y/n]? y
Extracting SDK.......................done
Setting it up...SDK has been successfully set up and is ready to be used.
Each time you wish to use the SDK in a new shell session, you need to source the environment setup script e.g.
 $ . /home/xxx/toolchain/environment-setup-aarch64-openeuler-linux
```

2.2）交叉工具链生效

```
cd /home/xxx/toolchain/
. environment-setup-aarch64-openeuler-linux
```

2.3）下载本工程以及驱动需要的头文件和库并编译(编译二进制在当前目录的output目录下)

```
git clone https://gitee.com/HiEuler/externed_device_sample.git
git clone https://gitee.com/HiEuler/hardware_driver.git
mkdir -p externed_device_sample/mpp/out
cp hardware_driver/drivers/lib.tar.gz externed_device_sample/mpp/out/
cp hardware_driver/drivers/include.tar.gz externed_device_sample/mpp/out/
cd externed_device_sample/mpp/out/
tar -zxvf lib.tar.gz
tar -zxvf include.tar.gz
cd -
cd externed_device_sample/
make
```

#### sample使用说明
参考每个sample的README.md

