# sample_mipi_vdec

# 介绍

当前sample_mipi_vdec是解码H265视频文件并四分屏输出到mipi屏的示例。

# 使用说明
mipi屏连接欧拉派开发板的转接板上。接线参考下图。
![](mipi_vdec_01.jpg)

编译成功后，将output/mipi_vdec上传到开发板/root/device_sample/mipi_vdec。

```
cd /root/device_sample/mipi_vdec
sd3403 ~ # ./sample_vdec 1920_1200.h265
start vo dhd0.
mipi intf sync = 48

_sample_test:press 'e' to exit; 'q' to query!;

  chn 0, stream file:1920_1200.h265, userbufsize: 3456000

  chn 1, stream file:1920_1200.h265, userbufsize: 3456000

  chn 2, stream file:1920_1200.h265, userbufsize: 3456000

  chn 3, stream file:1920_1200.h265, userbufsize: 3456000
q
 ---------------------------------------------------------------
 chn:0, type:265, start:1, decode_frames:17874, left_pics:0, left_bytes:0, left_frames:0, recv_frames:17874
 format_err:0,    pic_size_err_set:0,  stream_unsprt:0,  pack_err:0, set_pic_size_err:0,  ref_err_set:0,  pic_buf_size_err_set:0
 -----------------------------------------------------------------
 ---------------------------------------------------------------
 chn:1, type:265, start:1, decode_frames:17874, left_pics:0, left_bytes:0, left_frames:0, recv_frames:17874
 format_err:0,    pic_size_err_set:0,  stream_unsprt:0,  pack_err:0, set_pic_size_err:0,  ref_err_set:0,  pic_buf_size_err_set:0
 -----------------------------------------------------------------
 ---------------------------------------------------------------
 chn:2, type:265, start:1, decode_frames:17875, left_pics:0, left_bytes:0, left_frames:0, recv_frames:17875
 format_err:0,    pic_size_err_set:0,  stream_unsprt:0,  pack_err:0, set_pic_size_err:0,  ref_err_set:0,  pic_buf_size_err_set:0
 -----------------------------------------------------------------
 ---------------------------------------------------------------
 chn:3, type:265, start:1, decode_frames:17873, left_pics:0, left_bytes:0, left_frames:0, recv_frames:17873
 format_err:0,    pic_size_err_set:0,  stream_unsprt:0,  pack_err:0, set_pic_size_err:0,  ref_err_set:0,  pic_buf_size_err_set:0
 -----------------------------------------------------------------
```
mipi屏播放视频文件内容。
![](mipi_vdec_02.jpg)


# 注意事项
如果开发板上电后如果MIPI屏幕背光不亮，请先检查接线，接线没问题可以设置gpio4

```
bspmm 0x0102F00E0 0x1200
echo 4 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio4/direction
echo 1 > /sys/class/gpio/gpio4/value
```