


#!/bin/sh

# 检查是否传递了正确的参数
if [ $# -ne 1 ]; then
    echo "Usage: $0 <device_node>"
    exit 1
fi

# 获取传递的设备节点
device_node=$1

# 检查设备节点是否存在
if [ ! -e "$device_node" ]; then
    echo "Device node $device_node does not exist."
    exit 1
fi

echo "Device node: $device_node"

# 创建一个临时挂载点
mount_point=$(mktemp -d)
echo "Mounting $device_node to $mount_point"
 mount "$device_node" "$mount_point"

# 创建一个测试文件
test_file="$mount_point/testfile"
dd if=/dev/urandom of="$test_file" bs=1M count=200 > /dev/null 2>&1

# 测试读速度
echo "Testing read speed..."
read_speed=$(dd if="$test_file" of=/dev/null bs=1M iflag=direct 2>&1 | awk '/copied/ {print $(NF-1), $NF}')
echo "Read Speed: $read_speed"

# 测试写速度
echo "Testing write speed..."
write_speed=$(dd if=/dev/zero of="$test_file" bs=1M count=50 oflag=direct 2>&1 | awk '/copied/ {print $(NF-1), $NF}')
echo "Write Speed: $write_speed"

# 卸载 U 盘
echo "Unmounting $device_node"
 umount "$mount_point"
rm -r "$mount_point"

