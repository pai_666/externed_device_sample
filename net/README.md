# NET

## 一、例程简介

NET网络测速程序分为服务器（server）和客户端（clinet），在其他linux设备上运行测速客户端程序，在欧拉派上运行测速服务器程序

## 二、测试环境说明

### 1) 硬件准备

- 欧拉派
- PC
- 千兆网线

### 2) 软件环境

- 硬件准备中的PC需具备linux环境，且能通过桥接等方式，正确与欧拉派网络通信
- 通过gcc编译器编译net_clinet.c文件，并命名为net_clinet

## 三、硬件连接

如图所示为ETH0千兆网口

![image-20240219101557492](./README.assets/image-20240219101557492.png)

## 四、例程运行说明

在开始测试前，先检查pc端（电脑）是否正常与欧拉派网络通信

### 1）ETH0网口网络测速

__用千兆网线连接pc端和欧拉派ETH0网口__

在欧拉派上设置eth0网卡为192.168.1.2，开启eth0网卡，并运行./net_server命令

```c
ifconfig eth0 192.168.1.2
```

```c
ifconfig eth0 up
```

```c
./net_server
```

在pc端上设置网卡ip为192.168.1.33，运行./net_clinet命令

```c
./net_clinet
```

等待几秒钟后，客户端程序将会打印ETH0网口网络速率

__将欧拉派eth0网卡关闭__

```c
ifconfig eth0 down
```

### 2）ETH1网口网络测速

__用千兆网线连接pc端和欧拉派ETH1网口__

在欧拉派上设置eth1网卡为192.168.1.2，开启eth1网卡，并运行./net_server命令

```c
ifconfig eth1 192.168.1.2
```

```c
ifconfig eth1 up
```

```c
./net_server
```

在pc端上设置网卡ip为192.168.1.33，运行./net_clinet命令

```c
./net_clinet
```

等待几秒钟后，客户端程序将会打印ETH1网口网络速率







